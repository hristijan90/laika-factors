<?php

use Illuminate\Database\Seeder;


class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['female', 'male', 'other'];
        for($i = 0; $i < count($array); $i++)
        {
            $gender = new App\Gender;
            $gender->gender = $array[$i];
            $gender->save();
        }
    }
}
