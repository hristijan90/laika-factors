<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ExperienceTableSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
