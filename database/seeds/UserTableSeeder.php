<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 100; $i++)
        {
            $user = new App\User;
            $user->experience_id = rand(1, 5);
            $user->gender_id = rand(1, 3);
            $user->location_id = rand(1, 5);
            $user->importantToYou = '';
            $user->jobDefinition = rand(1, 10);
            $user->techStack = rand(1, 10);
            $user->teamMembers = rand(1, 10);
            $user->industry = rand(1, 10);
            $user->product = rand(1, 10);
            $user->salary = rand(1, 10);
            $user->stockOptions = rand(1, 10);
            $user->location = rand(1, 10);
            $user->commute = rand(1, 10);
            $user->companySize = rand(1, 10);
            $user->benefitsPerks = rand(1, 10);
            $user->save();
        }
    }
}
