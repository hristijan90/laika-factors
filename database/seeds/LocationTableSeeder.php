<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['macedonia', 'serbia', 'bulgaria', 'croatia', 'other'];
        for($i = 0; $i < count($array); $i++)
        {
            $location = new App\Location;
            $location->yourLocation = $array[$i];
            $location->save();
        }
    }
}
