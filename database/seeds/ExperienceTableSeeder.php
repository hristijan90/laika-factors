<?php

use Illuminate\Database\Seeder;

class ExperienceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Under_1Year','1_4Years', '4_7Years', '7_10Years', '10_plusYears'];
        for($i = 0; $i < count($array); $i++)
        {
            $experience = new App\Experience;
            $experience->experience = $array[$i];
            $experience->save();
        }
    }
}
