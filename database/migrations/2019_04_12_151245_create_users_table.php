<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('experience_id');
            $table->unsignedBigInteger('gender_id');
            $table->unsignedBigInteger('location_id');
            $table->string('importantToYou')->nullable();
            $table->tinyInteger('jobDefinition');
            $table->tinyInteger('techStack');
            $table->tinyInteger('teamMembers');
            $table->tinyInteger('industry');
            $table->tinyInteger('product');
            $table->tinyInteger('salary');
            $table->tinyInteger('stockOptions');
            $table->tinyInteger('location');
            $table->tinyInteger('commute');
            $table->tinyInteger('companySize');
            $table->tinyInteger('benefitsPerks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
