<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experience';

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
