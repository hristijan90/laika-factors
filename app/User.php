<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function experience()
    {
        return $this->belongsTo(Experience::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
