<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FormValidateRequest;
use App\User;
use App\Experience;
use App\Gender;
use App\Location;

class FormController extends Controller
{
    public function getForm()
    {
        return view('form');
    }

    public function store(FormValidateRequest $request)
    {
        $user = new User;
        $user->experience_id = $request->experience;
        $user->gender_id = $request->gender;
        $user->location_id = $request->yourLocation;
        $user->importantToYou = $request->importantToYou;
        $user->jobDefinition = $request->jobDefinition;
        $user->techStack = $request->techStack;
        $user->teamMembers = $request->teamMembers;
        $user->industry = $request->industry;
        $user->product = $request->product;
        $user->salary = $request->salary;
        $user->stockOptions = $request->stockOptions;
        $user->location = $request->location;
        $user->commute = $request->commute;
        $user->companySize = $request->companySize;
        $user->benefitsPerks = $request->benefitsPerks;
        $user->save();

        return redirect()->route('index');
    }

    public function goToIndex()
    {
        $count = User::count();

        $responses = $this->responses(strval($count));

        $averageSalary = round(User::avg('salary'), 1);
        $averageJobDefinition = round(User::avg('jobDefinition'), 1);
        $averageTechStack = round(User::avg('techStack'), 1);
        $averageTeamMembers = round(User::avg('teamMembers'), 1);
        $averageIndustry = round(User::avg('industry'), 1);
        $averageProduct = round(User::avg('product'), 1);
        $averageStockOptions = round(User::avg('stockOptions'), 1);
        $averageLocation = round(User::avg('location'), 1);
        $averageCommute = round(User::avg('commute'), 1);
        $averageCompanySize = round(User::avg('companySize'), 1);
        $averageBenefitsPerks = round(User::avg('benefitsPerks'), 1);


        $expUnd1 = User::select('id')->where('experience_id', 1)->get()->count();
        $exp14 = User::select('id')->where('experience_id', 2)->get()->count();
        $exp47 = User::select('id')->where('experience_id', 3)->get()->count();
        $exp710 = User::select('id')->where('experience_id', 4)->get()->count();
        $expUp10 = User::select('id')->where('experience_id', 5)->get()->count();

        $higherExp = $this->getHigher($expUnd1, $exp14, $exp47, $exp710, $expUp10);

        $expUnd1percent = $this->ExpPer($expUnd1, $higherExp);
        $exp14percent = $this->ExpPer($exp14, $higherExp);
        $exp47percent = $this->ExpPer($exp47, $higherExp);
        $exp710percent = $this->ExpPer($exp710, $higherExp);
        $expUp10percent = $this->ExpPer($expUp10, $higherExp);

        $salaryOverTime = $this->overTime('salary');
        $techOverTime = $this->overTime('techStack');
        $definitionOverTime = $this->overTime('jobDefinition');
        $teamOverTime = $this->overTime('teamMembers');
        $industryOverTime = $this->overTime('industry');
        $productOverTime = $this->overTime('product');
        $optionsOverTime = $this->overTime('stockOptions');
        $locationOverTime = $this->overTime('location');
        $commuteOverTime = $this->overTime('commute');
        $sizeOverTime = $this->overTime('companySize');
        $benefitsOverTime = $this->overTime('benefitsPerks');

        $countMale=  User::select('id')->where('gender_id', 2)->get()->count();
        $countFemale=  User::select('id')->where('gender_id', 1)->get()->count();
        $countOther=  User::select('id')->where('gender_id', 3)->get()->count();
    
        $salaryByGender= $this->byGender('salary');
        $techByGender= $this->byGender('techStack');
        $definitionByGender= $this->byGender('jobDefinition');
        $teamByGender= $this->byGender('teamMembers');
        $industryByGender= $this->byGender('industry');
        $productByGender= $this->byGender('product');
        $optionsByGender= $this->byGender('stockOptions');
        $locationByGender= $this->byGender('location');
        $commuteByGender= $this->byGender('commute');
        $sizeByGender= $this->byGender('companySize');
        $benefitsByGender= $this->byGender('benefitsPerks');

        $salaryByCountry=$this->byCountry('salary');
        $techByCountry= $this->byCountry('techStack');
        $definitionByCountry= $this->byCountry('jobDefinition');
        $teamByCountry= $this->byCountry('teamMembers');
        $industryByCountry= $this->byCountry('industry');
        $productByCountry= $this->byCountry('product');
        $optionsByCountry= $this->byCountry('stockOptions');
        $locationByCountry= $this->byCountry('location');
        $commuteByCountry= $this->byCountry('commute');
        $sizeByCountry= $this->byCountry('companySize');
        $benefitsByCountry= $this->byCountry('benefitsPerks');

        $countriesBySalary = $this->countriesByRating('salary');
        $countriesByTech = $this->countriesByRating('techStack');
        $countriesByDefinition = $this->countriesByRating('jobDefinition');
        $countriesByTeam = $this->countriesByRating('teamMembers');
        $countriesByIndustry = $this->countriesByRating('industry');
        $countriesByProduct = $this->countriesByRating('product');
        $countriesByOptions = $this->countriesByRating('stockOptions');
        $countriesByLocation = $this->countriesByRating('location');
        $countriesByCommute = $this->countriesByRating('commute');
        $countriesBySize = $this->countriesByRating('companySize');
        $countriesByBenefits = $this->countriesByRating('benefitsPerks');


        return view('index', compact(
            'responses',
            'averageSalary',
            'averageJobDefinition',
            'averageTechStack',
            'averageTeamMembers',
            'averageIndustry',
            'averageProduct',
            'averageStockOptions',
            'averageLocation',
            'averageCommute',
            'averageCompanySize',
            'averageBenefitsPerks',
            'expUnd1',
            'exp14',
            'exp47',
            'exp710',
            'expUp10',
            'count',
            'expUnd1percent',
            'exp14percent',
            'exp47percent',
            'exp710percent',
            'expUp10percent',
            'salaryOverTime',
            'techOverTime',
            'definitionOverTime',
            'teamOverTime',
            'industryOverTime',
            'productOverTime',
            'optionsOverTime',
            'locationOverTime',
            'commuteOverTime',
            'sizeOverTime',
            'benefitsOverTime',
            'countMale',
            'countFemale',
            'countOther',
            'salaryByGender',
            'techByGender',
            'definitionByGender',
            'teamByGender',
            'industryByGender',
            'productByGender',
            'optionsByGender',
            'locationByGender',
            'commuteByGender',
            'sizeByGender',
            'benefitsByGender',
            'salaryByCountry',
            'techByCountry',
            'definitionByCountry',
            'teamByCountry',
            'industryByCountry',
            'productByCountry',
            'optionsByCountry',
            'locationByCountry',
            'commuteByCountry',
            'sizeByCountry',
            'benefitsByCountry',
            'countriesBySalary',
            'countriesByTech',
            'countriesByDefinition',
            'countriesByTeam',
            'countriesByIndustry',
            'countriesByProduct',
            'countriesByOptions',
            'countriesByLocation',
            'countriesByCommute',
            'countriesBySize',
            'countriesByBenefits'
        ));
    }

    public function responses($string)
    {
        $resArray = [];
        for ($i = 0; $i <= strlen($string) + 1; $i++) {
            $letter = substr($string, 0, 1);
            $string = substr($string, 1);
            $resArray[] = $letter;
        }
        return $resArray;
    }

    public function ExpPer($num1, $num2)
    {
        $percent = $num1 * 100 / $num2;
        return round($percent, 1);
    }

    public function getHigher($num1, $num2, $num3, $num4, $num5)
    {
        $higher = $num1;
        if ($num2 > $higher) {
            $higher = $num2;
        }
        if ($num2 > $higher) {
            $higher = $num2;
        }
        if ($num3 > $higher) {
            $higher = $num2;
        }
        if ($num4 > $higher) {
            $higher = $num2;
        }
        if ($num5 > $higher) {
            $higher = $num2;
        }
        return $higher;
    }

    public function overTime($column)
    {
        $arr = [];
        $arr[] = round(User::select($column)->where('experience_id', 1)->avg($column),2);
        $arr[] = round(User::select($column)->where('experience_id', 2)->avg($column),2);
        $arr[] = round(User::select($column)->where('experience_id', 3)->avg($column),2);
        $arr[] = round(User::select($column)->where('experience_id', 4)->avg($column),2);
        $arr[] = round(User::select($column)->where('experience_id', 5)->avg($column),2);
        return $arr;
    }

    public function byGender($column)
    {
        $arr = [];
        $arr[] = round(User::select($column)->where('gender_id', 1)->avg($column),1);
        $arr[] = round(User::select($column)->where('gender_id', 2)->avg($column),1);
        $arr[] = round(User::select($column)->where('gender_id', 3)->avg($column),1);
        return $arr;
    }

    public function byCountry($column) {
        $arr = [];
        $arr[] = round(User::select($column)->where('location_id', 1)->avg($column),1);
        $arr[] = round(User::select($column)->where('location_id', 2)->avg($column),1);
        $arr[] = round(User::select($column)->where('location_id', 3)->avg($column),1);
        $arr[] = round(User::select($column)->where('location_id', 4)->avg($column),1);
        return $arr;
    }

    public function countriesByRating($column) {
        $countries = Location::where('id', 1)->orWhere('id', 2)->orWhere('id', 3)->orWhere('id', 4)->get();
        $countries = $countries->sortByDesc(function($item) use(&$column){
            return $item->users()->avg($column);
        });
        $countriesArray = [];
        foreach ($countries as $country)
        {
            $countriesArray[] = $country->yourLocation;
        }
        return $countriesArray;
    }
}
