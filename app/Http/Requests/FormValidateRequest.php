<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormValidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jobDefinition' => 'required|integer|min:1|max:10',
            'techStack' => 'required|integer|min:1|max:10',
            'teamMembers' => 'required|integer|min:1|max:10',
            'industry' => 'required|integer|min:1|max:10',
            'product' => 'required|integer|min:1|max:10',
            'salary' => 'required|integer|min:1|max:10',
            'stockOptions' => 'required|integer|min:1|max:10',
            'location' => 'required|integer|min:1|max:10',
            'commute' => 'required|integer|min:1|max:10',
            'companySize' => 'required|integer|min:1|max:10',
            'benefitsPerks' => 'required|integer|min:1|max:10',
            'importantToYou' => 'nullable',
            'experience' => 'required',
            'gender' => 'required',
            'yourLocation' => 'required'
        ];
    }
}