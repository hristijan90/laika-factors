

let sallaryDataSet = $('.barSallary');
let techDataSet = $('.barTech');
let definitionDataSet = $('.barDefinition');
let teamDataSet = $('.barTeam');
let industryDataSet = $('.barIndustry');
let productDataSet = $('.barProduct');
let optionsDataSet = $('.barOptions');
let locationDataSet = $('.barLocation');
let commuteDataSet = $('.barCommute');
let orgDataSet = $('.barOrg');
let benefitsDataSet = $('.barBenefits');

let underOne = $('.bar-one');
let oneFour = $('.bar-two');
let fourSeven = $('.bar-three');
let sevenTen = $('.bar-four');
let tenPlus = $('.bar-five');

let barSalOne = $('.bar1-salary');
let barSalTwo = $('.bar2-salary');
let barSalThree = $('.bar3-salary');

let barTechOne = $('.bar1-tech');
let barTechTwo = $('.bar2-tech');
let barTechThree = $('.bar3-tech');

let barDefOne = $('.bar1-def');
let barDefTwo = $('.bar2-def');
let barDefthree = $('.bar3-def');

let barTeamOne = $('.bar1-team');
let barTeamTwo = $('.bar2-team');
let barTeamthree = $('.bar3-team');

let barIndOne = $('.bar1-ind');
let barIndTwo = $('.bar2-ind');
let barIndthree = $('.bar3-ind');

let barProOne = $('.bar1-pro');
let barProTwo = $('.bar2-pro');
let barProthree = $('.bar3-pro');

let barOptOne = $('.bar1-opt');
let barOptTwo = $('.bar2-opt');
let barOptthree = $('.bar3-opt');

let barLocOne = $('.bar1-loc');
let barLocTwo = $('.bar2-loc');
let barLocthree = $('.bar3-loc');

let barComOne = $('.bar1-com');
let barComTwo = $('.bar2-com');
let barComthree = $('.bar3-com');

let barOrgOne = $('.bar1-org');
let barOrgTwo = $('.bar2-org');
let barOrgthree = $('.bar3-org');

let barBenOne = $('.bar1-ben');
let barBenTwo = $('.bar2-ben');
let barBenthree = $('.bar3-ben');

// FLAGS CHARTS
let flagSalMk = $('.flags-sal-mk');
let flagSalCr = $('.flags-sal-cr');
let flagSalBg = $('.flags-sal-bg');
let flagSalRs = $('.flags-sal-rs');
let salMk = $('.sal-mk');
let salHr = $('.sal-hr');
let salBg = $('.sal-bg');
let salRs = $('.sal-rs');

let flagTechMk = $('.flags-tech-mk');
let flagTechCr = $('.flags-tech-cr');
let flagTechBg = $('.flags-tech-bg');
let flagTechRs = $('.flags-tech-rs');
let techMk = $('.tech-mk');
let techHr = $('.tech-hr');
let techBg = $('.tech-bg');
let techRs = $('.tech-rs');

flagTechMk.css({ 'width': '50%' });
techMk.attr("data-value", "50");
flagTechCr.css({ 'width': '90%' });
techHr.attr("data-value", "90");
flagTechBg.css({ 'width': '80%' });
techBg.attr("data-value", "80");
flagTechRs.css({ 'width': '60%' });
techRs.attr("data-value", "60");

let flagDefMk = $('.flags-def-mk');
let flagDefCr = $('.flags-def-cr');
let flagDefBg = $('.flags-def-bg');
let flagDefRs = $('.flags-def-rs');
let defMk = $('.def-mk');
let defHr = $('.def-hr');
let defBg = $('.def-bg');
let defRs = $('.def-rs');

let flagTeamMk = $('.flags-team-mk');
let flagTeamCr = $('.flags-team-cr');
let flagTeamBg = $('.flags-team-bg');
let flagTeamRs = $('.flags-team-rs');
let teamMk = $('.team-mk');
let teamHr = $('.team-hr');
let teamBg = $('.team-bg');
let teamRs = $('.team-rs');

let flagIndMk = $('.flags-ind-mk');
let flagIndCr = $('.flags-ind-cr');
let flagIndBg = $('.flags-ind-bg');
let flagIndRs = $('.flags-ind-rs');
let indMk = $('.ind-mk');
let indHr = $('.ind-hr');
let indBg = $('.ind-bg');
let indRs = $('.ind-rs');

let flagProMk = $('.flags-pro-mk');
let flagProCr = $('.flags-pro-cr');
let flagProBg = $('.flags-pro-bg');
let flagProRs = $('.flags-pro-rs');
let proMk = $('.pro-mk');
let proHr = $('.pro-hr');
let proBg = $('.pro-bg');
let proRs = $('.pro-rs');

let flagComMk = $('.flags-com-mk');
let flagComCr = $('.flags-com-cr');
let flagComBg = $('.flags-com-bg');
let flagComRs = $('.flags-com-rs');
let comMk = $('.com-mk');
let comHr = $('.com-hr');
let comBg = $('.com-bg');
let comRs = $('.com-rs');

let flagOrgMk = $('.flags-org-mk');
let flagOrgCr = $('.flags-org-cr');
let flagOrgBg = $('.flags-org-bg');
let flagOrgRs = $('.flags-org-rs');
let orgMk = $('.org-mk');
let orgHr = $('.org-hr');
let orgBg = $('.org-bg');
let orgRs = $('.org-rs');

let flagBenMk = $('.flags-ben-mk');
let flagBenCr = $('.flags-ben-cr');
let flagBenBg = $('.flags-ben-bg');
let flagBenRs = $('.flags-ben-rs');
let benMk = $('.ben-mk');
let benHr = $('.ben-hr');
let benBg = $('.ben-bg');
let benRs = $('.ben-rs');





