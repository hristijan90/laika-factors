<!DOCTYPE html>
<html>

<head>
	<title></title>
	<meta name="keywords">
	<meta name="author" content="Rosik Stefan">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">

	<!-- Latest compiled and minified BOOTSTRAP CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- Local CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/form.css') }}">


	<!-- Font-awesome 4.7 cdn -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
		integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

</head>

<body>


	<div class="wrapper">
		<form method="POST" action="{{ route('post.form') }}">
			<div id="pb">
				<div id="progress">

				</div>
				<button class="progressText">0-15 Answered</button>
			</div>
			<div id="1" class="que question1">
				<h2 class="questionHeader q1">Job Definition *</h2>
				<h4 class="text-muted">What the job entails and how you will be spending your time.</h4>
				<div class="mainDivChoices">
					<label for="JD1">1</label>
					<input name="jobDefinition" id="JD1" type='radio' class="btn" value="1">
					<label for="JD2">2</label>
					<input name="jobDefinition" id="JD2" type='radio' class="btn" value="2">
					<label for="JD3">3</label>
					<input name="jobDefinition" id="JD3" type='radio' class="btn" value="3">
					<label for="JD4">4</label>
					<input name="jobDefinition" id="JD4" type='radio' class="btn" value="4">
					<label for="JD5">5</label>
					<input name="jobDefinition" id="JD5" type='radio' class="btn" value="5">
					<label for="JD6">6</label>
					<input name="jobDefinition" id="JD6" type='radio' class="btn" value="6">
					<label for="JD7">7</label>
					<input name="jobDefinition" id="JD7" type='radio' class="btn" value="7">
					<label for="JD8">8</label>
					<input name="jobDefinition" id="JD8" type='radio' class="btn" value="8">
					<label for="JD9">9</label>
					<input name="jobDefinition" id="JD9" type='radio' class="btn" value="9">
					<label for="JD10">10</label>
					<input name="jobDefinition" id="JD10" type='radio' class="btn" value="10">
				</div>
				<span class="SPANs">Irrelevant</span>
				<span class="text-right SPANs">Important</span>
			</div>
			<div id="2" class="que question2">
				<h2 class="questionHeader q2">Tech Stack *</h2>
				<h4 class="text-muted">The technologies and programming languages you will need to work with.</h4>
				<div class="mainDivChoices">
					<label for="TS1">1</label>
					<input name="techStack" id="TS1" type='radio' class="btn" value="1">
					<label for="TS2">2</label>
					<input name="techStack" id="TS2" type='radio' class="btn" value="2">
					<label for="TS3">3</label>
					<input name="techStack" id="TS3" type='radio' class="btn" value="3">
					<label for="TS4">4</label>
					<input name="techStack" id="TS4" type='radio' class="btn" value="4">
					<label for="TS5">5</label>
					<input name="techStack" id="TS5" type='radio' class="btn" value="5">
					<label for="TS6">6</label>
					<input name="techStack" id="TS6" type='radio' class="btn" value="6">
					<label for="TS7">7</label>
					<input name="techStack" id="TS7" type='radio' class="btn" value="7">
					<label for="TS8">8</label>
					<input name="techStack" id="TS8" type='radio' class="btn" value="8">
					<label for="TS9">9</label>
					<input name="techStack" id="TS9" type='radio' class="btn" value="9">
					<label for="TS10">10</label>
					<input name="techStack" id="TS10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="3" class="que question3">
				<h2 class="questionHeader q3">Team Members *</h2>
				<h4 class="text-muted">The technologies and programming languages you will need to work with.</h4>
				<div class="mainDivChoices">
					<label for="TM1">1</label>
					<input name="teamMembers" id="TM1" type='radio' class="btn" value="1">
					<label for="TM2">2</label>
					<input name="teamMembers" id="TM2" type='radio' class="btn" value="2">
					<label for="TM3">3</label>
					<input name="teamMembers" id="TM3" type='radio' class="btn" value="3">
					<label for="TM4">4</label>
					<input name="teamMembers" id="TM4" type='radio' class="btn" value="4">
					<label for="TM5">5</label>
					<input name="teamMembers" id="TM5" type='radio' class="btn" value="5">
					<label for="TM6">6</label>
					<input name="teamMembers" id="TM6" type='radio' class="btn" value="6">
					<label for="TM7">7</label>
					<input name="teamMembers" id="TM7" type='radio' class="btn" value="7">
					<label for="TM8">8</label>
					<input name="teamMembers" id="TM8" type='radio' class="btn" value="8">
					<label for="TM9">9</label>
					<input name="teamMembers" id="TM9" type='radio' class="btn" value="9">
					<label for="TM10">10</label>
					<input name="teamMembers" id="TM10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="4" class="que question4">
				<h2 class="questionHeader q4">Industry *</h2>
				<h4 class="text-muted">What business sector the company operates in.</h4>
				<div class="mainDivChoices">
					<label for="I1">1</label>
					<input name="industry" id="I1" type='radio' class="btn" value="1">
					<label for="I2">2</label>
					<input name="industry" id="I2" type='radio' class="btn" value="2">
					<label for="I3">3</label>
					<input name="industry" id="I3" type='radio' class="btn" value="3">
					<label for="I4">4</label>
					<input name="industry" id="I4" type='radio' class="btn" value="4">
					<label for="I5">5</label>
					<input name="industry" id="I5" type='radio' class="btn" value="5">
					<label for="I6">6</label>
					<input name="industry" id="I6" type='radio' class="btn" value="6">
					<label for="I7">7</label>
					<input name="industry" id="I7" type='radio' class="btn" value="7">
					<label for="I8">8</label>
					<input name="industry" id="I8" type='radio' class="btn" value="8">
					<label for="I9">9</label>
					<input name="industry" id="I9" type='radio' class="btn" value="9">
					<label for="I10">10</label>
					<input name="industry" id="I10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="5" class="que question5">
				<h2 class="questionHeader q5">Product *</h2>
				<h4 class="text-muted">What the product is like.</h4>
				<div class="mainDivChoices">
					<label for="P1">1</label>
					<input name="product" id="P1" type='radio' class="btn" value="1">
					<label for="P2">2</label>
					<input name="product" id="P2" type='radio' class="btn" value="2">
					<label for="P3">3</label>
					<input name="product" id="P3" type='radio' class="btn" value="3">
					<label for="P4">4</label>
					<input name="product" id="P4" type='radio' class="btn" value="4">
					<label for="P5">5</label>
					<input name="product" id="P5" type='radio' class="btn" value="5">
					<label for="P6">6</label>
					<input name="product" id="P6" type='radio' class="btn" value="6">
					<label for="P7">7</label>
					<input name="product" id="P7" type='radio' class="btn" value="7">
					<label for="P8">8</label>
					<input name="product" id="P8" type='radio' class="btn" value="8">
					<label for="P9">9</label>
					<input name="product" id="P9" type='radio' class="btn" value="9">
					<label for="P10">10</label>
					<input name="product" id="P10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="6" class="que question6">
				<h2 class="questionHeader q6">Salary *</h2>
				<div class="mainDivChoices">
					<label for="S1">1</label>
					<input name="salary" id="S1" type='radio' class="btn" value="1">
					<label for="S2">2</label>
					<input name="salary" id="S2" type='radio' class="btn" value="2">
					<label for="S3">3</label>
					<input name="salary" id="S3" type='radio' class="btn" value="3">
					<label for="S4">4</label>
					<input name="salary" id="S4" type='radio' class="btn" value="4">
					<label for="S5">5</label>
					<input name="salary" id="S5" type='radio' class="btn" value="5">
					<label for="S6">6</label>
					<input name="salary" id="S6" type='radio' class="btn" value="6">
					<label for="S7">7</label>
					<input name="salary" id="S7" type='radio' class="btn" value="7">
					<label for="S8">8</label>
					<input name="salary" id="S8" type='radio' class="btn" value="8">
					<label for="S9">9</label>
					<input name="salary" id="S9" type='radio' class="btn" value="9">
					<label for="S10">10</label>
					<input name="salary" id="S10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="7" class="que question7">
				<h2 class="questionHeader q7">Stock Options *</h2>
				<div class="mainDivChoices">
					<label for="SO1">1</label>
					<input name="stockOptions" id="SO1" type='radio' class="btn" value="1">
					<label for="SO2">2</label>
					<input name="stockOptions" id="SO2" type='radio' class="btn" value="2">
					<label for="SO3">3</label>
					<input name="stockOptions" id="SO3" type='radio' class="btn" value="3">
					<label for="SO4">4</label>
					<input name="stockOptions" id="SO4" type='radio' class="btn" value="4">
					<label for="SO5">5</label>
					<input name="stockOptions" id="SO5" type='radio' class="btn" value="5">
					<label for="SO6">6</label>
					<input name="stockOptions" id="SO6" type='radio' class="btn" value="6">
					<label for="SO7">7</label>
					<input name="stockOptions" id="SO7" type='radio' class="btn" value="7">
					<label for="SO8">8</label>
					<input name="stockOptions" id="SO8" type='radio' class="btn" value="8">
					<label for="SO9">9</label>
					<input name="stockOptions" id="SO9" type='radio' class="btn" value="9">
					<label for="SO10">10</label>
					<input name="stockOptions" id="SO10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="8" class="que question8">
				<h2 class="questionHeader q8">Location *</h2>
				<h4 class="text-muted">The location you will work out of.</h4>
				<div class="mainDivChoices">
					<label for="L1">1</label>
					<input name="location" id="L1" type='radio' class="btn" value="1">
					<label for="L2">2</label>
					<input name="location" id="L2" type='radio' class="btn" value="2">
					<label for="L3">3</label>
					<input name="location" id="L3" type='radio' class="btn" value="3">
					<label for="L4">4</label>
					<input name="location" id="L4" type='radio' class="btn" value="4">
					<label for="L5">5</label>
					<input name="location" id="L5" type='radio' class="btn" value="5">
					<label for="L6">6</label>
					<input name="location" id="L6" type='radio' class="btn" value="6">
					<label for="L7">7</label>
					<input name="location" id="L7" type='radio' class="btn" value="7">
					<label for="L8">8</label>
					<input name="location" id="L8" type='radio' class="btn" value="8">
					<label for="L9">9</label>
					<input name="location" id="L9" type='radio' class="btn" value="9">
					<label for="L10">10</label>
					<input name="location" id="L10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="9" class="que question9">
				<h2 class="questionHeader q9">Commute *</h2>
				<h4 class="text-muted">How long your journey to work will be.</h4>
				<div class="mainDivChoices">
					<label for="C1">1</label>
					<input name="commute" id="C1" type='radio' class="btn" value="1">
					<label for="C2">2</label>
					<input name="commute" id="C2" type='radio' class="btn" value="2">
					<label for="C3">3</label>
					<input name="commute" id="C3" type='radio' class="btn" value="3">
					<label for="C4">4</label>
					<input name="commute" id="C4" type='radio' class="btn" value="4">
					<label for="C5">5</label>
					<input name="commute" id="C5" type='radio' class="btn" value="5">
					<label for="C6">6</label>
					<input name="commute" id="C6" type='radio' class="btn" value="6">
					<label for="C7">7</label>
					<input name="commute" id="C7" type='radio' class="btn" value="7">
					<label for="C8">8</label>
					<input name="commute" id="C8" type='radio' class="btn" value="8">
					<label for="C9">9</label>
					<input name="commute" id="C9" type='radio' class="btn" value="9">
					<label for="C10">10</label>
					<input name="commute" id="C10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="10" class="que question10">
				<h2 class="questionHeader q10">Company Size *</h2>
				<h4 class="text-muted">How many employees the company has.</h4>
				<div class="mainDivChoices">
					<label for="CS1">1</label>
					<input name="companySize" id="CS1" type='radio' class="btn" value="1">
					<label for="CS2">2</label>
					<input name="companySize" id="CS2" type='radio' class="btn" value="2">
					<label for="CS3">3</label>
					<input name="companySize" id="CS3" type='radio' class="btn" value="3">
					<label for="CS4">4</label>
					<input name="companySize" id="CS4" type='radio' class="btn" value="4">
					<label for="CS5">5</label>
					<input name="companySize" id="CS5" type='radio' class="btn" value="5">
					<label for="CS6">6</label>
					<input name="companySize" id="CS6" type='radio' class="btn" value="6">
					<label for="CS7">7</label>
					<input name="companySize" id="CS7" type='radio' class="btn" value="7">
					<label for="CS8">8</label>
					<input name="companySize" id="CS8" type='radio' class="btn" value="8">
					<label for="CS9">9</label>
					<input name="companySize" id="CS9" type='radio' class="btn" value="9">
					<label for="CS10">10</label>
					<input name="companySize" id="CS10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="11" class="que question11">
				<h2 class="questionHeader q11">Benefits + Perks *</h2>
				<h4 class="text-muted">Inclusion of Pension, Medical Care, Car, etc.</h4>
				<div class="mainDivChoices">
					<label for="BP1">1</label>
					<input name="benefitsPerks" id="BP1" type='radio' class="btn" value="1">
					<label for="BP2">2</label>
					<input name="benefitsPerks" id="BP2" type='radio' class="btn" value="2">
					<label for="BP3">3</label>
					<input name="benefitsPerks" id="BP3" type='radio' class="btn" value="3">
					<label for="BP4">4</label>
					<input name="benefitsPerks" id="BP4" type='radio' class="btn" value="4">
					<label for="BP5">5</label>
					<input name="benefitsPerks" id="BP5" type='radio' class="btn" value="5">
					<label for="BP6">6</label>
					<input name="benefitsPerks" id="BP6" type='radio' class="btn" value="6">
					<label for="BP7">7</label>
					<input name="benefitsPerks" id="BP7" type='radio' class="btn" value="7">
					<label for="BP8">8</label>
					<input name="benefitsPerks" id="BP8" type='radio' class="btn" value="8">
					<label for="BP9">9</label>
					<input name="benefitsPerks" id="BP9" type='radio' class="btn" value="9">
					<label for="BP10">10</label>
					<input name="benefitsPerks" id="BP10" type='radio' class="btn" value="10">
				</div>
				<span>Irrelevant</span>
				<span class="text-right">Important</span>
			</div>
			<div id="12" class="que question12">
				<h2 class="questionHeader q12">Is there anything else that is important to you when considering a new
					role that we missed?</h2>
				<div>
					<input name="importantToYou" type="text" class="input12 key" placeholder="Type your answer here...">
					<button type="button" class="btn btn12 ">Submit</button>
				</div>
			</div>
			<div id="13" class="que question13">
				<h2 class="questionHeader q13">How many years of experience do you have as an engineer.</h2>
				<div class="mainDivChoices LabelsRadios">
					<label for="Under_1Year">Under 1 year</label>
					<input name="experience" type="radio" id="Under_1Year" value="1">
					<label for="1_4Years">1-4 years</label>
					<input name="experience" type="radio" id="1_4Years" value="2">
					<label for="4_7Years">4-7 years</label>
					<input name="experience" type="radio" id="4_7Years" value="3">
					<label for="7_10Years">7-10 years</label>
					<input name="experience" type="radio" id="7_10Years" value="4">
					<label for="10plus">10+ years</label>
					<input name="experience" type="radio" id="10plus" value="5">
				</div>
			</div>
			<div id="14" class="que question14">
				<h2 class="questionHeader q14">Gender *</h2>
				<div class="mainDivChoices LabelsRadios">
					<label for="Female">Female</label>
					<input name="gender" type="radio" id="Female" value="1">
					<label for="Male">Male</label>
					<input name="gender" type="radio" id="Male" value="2">
					<label for="Other">Other</label>
					<input name="gender" type="radio" id="Other" value="3">
				</div>
			</div>
			<div id="15" class="que question15">
				<h2 class="questionHeader q15">Location *</h2>
				<div class="mainDivChoices LabelsRadios LabelsRadiosLast">
					<label for="MK">Macedonia</label>
					<input name="yourLocation" type="radio" id="MK" value="1">
					<label for="SR">Serbia</label>
					<input name="yourLocation" type="radio" id="SR" value="2">
					<label for="BG">Bulgaria</label>
					<input name="yourLocation" type="radio" id="BG" value="3">
					<label for="CR">Croatia</label>
					<input name="yourLocation" type="radio" id="CR" value="4">
					<label for="OT">Other</label>
					<input name="yourLocation" type="radio" id="OT" value="5">

				</div>
			</div>
			@csrf
		</form>
	</div>

	<!-- jQuery library -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.2/rangeslider.min.js"></script>
	<script src="{{ asset('js/ButtonsValueForm.js') }}"></script>
</body>

</html>